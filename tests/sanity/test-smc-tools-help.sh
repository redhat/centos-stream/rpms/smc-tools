#!/usr/bin/env sh
#
# smc-tools gating
#
# verify that LD_PRELOAD works
#


TESTS+=(test_smc_tools_ld_preload)

DESCRIPTION_test_smc_tools_ld_preload=(
        "Verify that smc-tools's libsmc-preload.so can be LD_PRELOAD-ed."
)

function test_smc_tools_ld_preload()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

	echo > $stdout_log
	echo > $stderr_log

	echo "# Calling LD_PRELOAD=\"$PLD_LD\" ping -4 google.com -c 1" \
		>> $stdout_log
	LD_PRELOAD="$PLD_LD" ping -4 google.com -c 1 >> $stdout_log 2>> $stderr_log

	if test $? -gt 0
	then
		echo
		echo "ERROR: Failed with non-zero return code." >&2

		echo "STDOUT {"
		cat $stdout_log
		echo "}"

		echo "STDERR {"
		cat $stderr_log
		echo "}"

		return 1
	fi
}
