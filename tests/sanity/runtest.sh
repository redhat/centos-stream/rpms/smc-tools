#!/usr/bin/env bash

function cleanup()
{
	rlPhaseStartCleanup
	rm -rf "${TEMPFILES[@]}"
	rlPhaseEnd
}
trap cleanup 0 1 9 15

# --- Globals -----------------------------------------------------------------

SCRIPT_ROOT="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

# A list of temporary files; used by cleanup to delete on signals 0 1 9 15.
TEMPFILES=()

# A list of dependencies to include.
INCLUDES=()

# A list of files containing test definitions.
# These are auto-discovered using test-*.sh pattern.
TESTS_FILES=()

# A list of tests to run.
# These are automatically added by test files.
TESTS=()

# The following can be overriden to force a particular setting.

# RPM_BIN_DIR is not defined
# RPM_DATA_DIR is not defined
# KSC_BIN is not defined
# MOD_PATH is not defined
# MANUAL is not defined

# --- Initialization ----------------------------------------------------------

echo
echo " :: smc-tools Gating for RHEL"
echo

# Requires: restraint-rhts
. /usr/bin/rhts-environment.sh || exit 1
# Requires: beakerlib
. /usr/share/beakerlib/beakerlib.sh || exit 1

INCLUDES+=("$SCRIPT_ROOT/common-tests.sh")

TESTS_FILES+=("$SCRIPT_ROOT/test-"*".sh")

# --- bkr journal -------------------------------------------------------------

rlJournalStart

rlPhaseStartSetup

# --- Load dependencies -------------------------------------------------------

for path in ${INCLUDES[@]} ${TESTS_FILES[@]}
do
	if ! test -r $path
	then
		rlFail "Path \`$path' does not exist or is not readable"
		exit 1
	fi
        source $path && {
		rlPass "File \`$(basename "$path")' loaded."
	} || {
		rlFail "Unable to load \`$path'."
		exit 1
	}
done

# --- Temporary files ---------------------------------------------------------

echo ":: Initialization: Temporary files."

__stdout_log=$(mktemp -p /tmp smc-tools-test-stdout.XXXXX)
TEMPFILES+=("$__stdout_log")

__stderr_log=$(mktemp -p /tmp smc-tools-test-stderr.XXXXX)
TEMPFILES+=("$__stderr_log")

# --- Evaluate RPM-specific macros --------------------------------------------
# This is required not to hardcode install location should %{_bindir} and
# %{_datadir} be changed.

echo ":: Initialization: Evaluating RPM macros."

if test -z "$RPM_BIN_DIR"
then
        RPM_BIN_DIR="$(rpm --eval '%{_bindir}')"
        echo "  * RPM %{_bindir} determined as: $RPM_BIN_DIR"
fi

if test -z "$RPM_DATA_DIR"
then
        RPM_DATA_DIR="$(rpm --eval '%{_datadir}')"
        echo "  * RPM %{_datadir} determined as: $RPM_DATA_DIR"
fi

if test -z "$RPM_LIB_DIR"
then
        RPM_LIB_DIR="$(rpm --eval '%{_libdir}')"
        echo "  * RPM %{_libdir} determined as: $RPM_LIB_DIR"
fi

# --- Determine scm location --------------------------------------------------

if test -z "$PLD_LD"
then
	PLD_LD="$RPM_LIB_DIR/libsmc-preload.so"
	echo ":: libsmc-preload.so determined as: $PLD_LD"
fi

rlPass "Initialization passed."

rlPhaseEnd

# --- Run tests ---------------------------------------------------------------

overall_status=0
for test in ${TESTS[@]}
do
        run_test $test "$__stdout_log" "$__stderr_log"
        if test $? -gt 0
        then
                overall_status=1
        fi
done

rlPhaseStartTest

if test $overall_status -gt 0
then
        rlFail "Some tests failed."
        exit 1
else
        rlPass "All tests passed."
fi
echo

rlPhaseEnd

rlJournalPrintText

rlJournalEnd

